package com.starzplay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(value = {"com.starzplay"})
public class StarzplayApplication {

	public static void main(String[] args) {
		SpringApplication.run(StarzplayApplication.class, args);
	}
}
