
package com.starzplay.service.gateway.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonAutoDetect
public class Credit {

    @JsonProperty("characterName")
    private String characterName;
    @JsonProperty("creditType")
    private String creditType;
    @JsonProperty("isInactive")
    private Boolean isInactive;
    @JsonProperty("order")
    private Long order;
    @JsonProperty("personId")
    private String personId;
    @JsonProperty("personName")
    private String personName;

}
