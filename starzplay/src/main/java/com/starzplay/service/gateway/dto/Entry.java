
package com.starzplay.service.gateway.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonAutoDetect
public class Entry {

    @JsonProperty("id")
    private String id;
    @JsonProperty("guid")
    private String guid;
    @JsonProperty("updated")
    private Long updated;
    @JsonProperty("title")
    private String title;
    @JsonProperty("description")
    private Object description;
    @JsonProperty("added")
    private Long added;
    @JsonProperty("approved")
    private Boolean approved;
    @JsonProperty("credits")
    private List<Credit> credits = null;
    @JsonProperty("descriptionLocalized")
    private DescriptionLocalized descriptionLocalized;
    @JsonProperty("displayGenre")
    private Object displayGenre;
    @JsonProperty("editorialRating")
    private Object editorialRating;
    @JsonProperty("imageMediaIds")
    private List<ImageMediaId> imageMediaIds = null;
    @JsonProperty("isAdult")
    private Object isAdult;
    @JsonProperty("languages")
    private List<String> languages = null;
    @JsonProperty("longDescription")
    private String longDescription;
    @JsonProperty("longDescriptionLocalized")
    private LongDescriptionLocalized longDescriptionLocalized;
    @JsonProperty("programType")
    private String programType;
    @JsonProperty("ratings")
    private List<Object> ratings = null;
    @JsonProperty("seriesEpisodeNumber")
    private Object seriesEpisodeNumber;
    @JsonProperty("seriesId")
    private Object seriesId;
    @JsonProperty("shortDescription")
    private String shortDescription;
    @JsonProperty("shortDescriptionLocalized")
    private ShortDescriptionLocalized shortDescriptionLocalized;
    @JsonProperty("tagIds")
    private List<String> tagIds = null;
    @JsonProperty("tags")
    private List<Tag> tags = null;
    @JsonProperty("thumbnails")
    private Thumbnails thumbnails;
    @JsonProperty("titleLocalized")
    private TitleLocalized titleLocalized;
    @JsonProperty("tvSeasonEpisodeNumber")
    private Object tvSeasonEpisodeNumber;
    @JsonProperty("tvSeasonId")
    private Object tvSeasonId;
    @JsonProperty("tvSeasonNumber")
    private Object tvSeasonNumber;
    @JsonProperty("year")
    private Integer year;
    @JsonProperty("media")
    private List<Medium> media = null;
    @JsonProperty("peg$ISOcountryOfOrigin")
    private String peg$ISOcountryOfOrigin;
    @JsonProperty("peg$arAgeRating")
    private Long peg$arAgeRating;
    @JsonProperty("peg$arContentRating")
    private String peg$arContentRating;
    @JsonProperty("peg$availableInSection")
    private String peg$availableInSection;
    @JsonProperty("peg$contentClassification")
    private String peg$contentClassification;
    @JsonProperty("peg$contractName")
    private String peg$contractName;
    @JsonProperty("peg$countryOfOrigin")
    private String peg$countryOfOrigin;
    @JsonProperty("peg$priorityLevel")
    private String peg$priorityLevel;
    @JsonProperty("peg$priorityLevelActionandAdventure")
    private String peg$priorityLevelActionandAdventure;
    @JsonProperty("peg$priorityLevelArabic")
    private String peg$priorityLevelArabic;
    @JsonProperty("peg$priorityLevelChildrenandFamily")
    private String peg$priorityLevelChildrenandFamily;
    @JsonProperty("peg$priorityLevelComedy")
    private String peg$priorityLevelComedy;
    @JsonProperty("peg$priorityLevelDisney")
    private String peg$priorityLevelDisney;
    @JsonProperty("peg$priorityLevelDisneyKids")
    private String peg$priorityLevelDisneyKids;
    @JsonProperty("peg$priorityLevelDrama")
    private String peg$priorityLevelDrama;
    @JsonProperty("peg$priorityLevelKids")
    private String peg$priorityLevelKids;
    @JsonProperty("peg$priorityLevelKidsAction")
    private String peg$priorityLevelKidsAction;
    @JsonProperty("peg$priorityLevelKidsFunandAdventure")
    private String peg$priorityLevelKidsFunandAdventure;
    @JsonProperty("peg$priorityLevelKidsMagicandDreams")
    private String peg$priorityLevelKidsMagicandDreams;
    @JsonProperty("peg$priorityLevelKidsPreschool")
    private String peg$priorityLevelKidsPreschool;
    @JsonProperty("peg$priorityLevelRomance")
    private String peg$priorityLevelRomance;
    @JsonProperty("peg$priorityLevelThriller")
    private String peg$priorityLevelThriller;
    @JsonProperty("peg$productCode")
    private String peg$productCode;
    @JsonProperty("peg$programMediaAvailability")
    private String peg$programMediaAvailability;

}
