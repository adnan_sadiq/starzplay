
package com.starzplay.service.gateway.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonAutoDetect
public class Release {

    @JsonProperty("pid")
    private String pid;
    @JsonProperty("url")
    private String url;
    @JsonProperty("restrictionId")
    private String restrictionId;

}
