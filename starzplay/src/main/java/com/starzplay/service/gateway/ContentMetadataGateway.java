package com.starzplay.service.gateway;


import com.starzplay.service.gateway.dto.ContentMetadataDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Adnan Sadiq.
 */
@Component
public class ContentMetadataGateway {

    @Value("${endpoints.service.content-metadata}")
    private String contentsMetadataServiceEndpoint;

    public ContentMetadataDTO getContentsMetadata() {

        RestTemplate restTemplate = new RestTemplate();
        ContentMetadataDTO metadataDTO = restTemplate.getForObject(contentsMetadataServiceEndpoint, ContentMetadataDTO.class);
        return metadataDTO;
    }

}
