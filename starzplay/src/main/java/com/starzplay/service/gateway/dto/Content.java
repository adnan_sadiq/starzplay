
package com.starzplay.service.gateway.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonAutoDetect
public class Content {

    @JsonProperty("bitrate")
    private Integer bitrate;
    @JsonProperty("duration")
    private Double duration;
    @JsonProperty("format")
    private String format;
    @JsonProperty("height")
    private Long height;
    @JsonProperty("language")
    private String language;
    @JsonProperty("width")
    private Long width;
    @JsonProperty("id")
    private String id;
    @JsonProperty("guid")
    private String guid;
    @JsonProperty("assetTypeIds")
    private List<String> assetTypeIds = null;
    @JsonProperty("assetTypes")
    private List<String> assetTypes = null;
    @JsonProperty("downloadUrl")
    private String downloadUrl;
    @JsonProperty("releases")
    private List<Release> releases = null;
    @JsonProperty("serverId")
    private String serverId;
    @JsonProperty("streamingUrl")
    private String streamingUrl;
    @JsonProperty("protectionScheme")
    private String protectionScheme;

}
