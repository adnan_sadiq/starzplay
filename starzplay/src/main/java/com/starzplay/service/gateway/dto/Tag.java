
package com.starzplay.service.gateway.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonAutoDetect
public class Tag {

    @JsonProperty("scheme")
    private String scheme;
    @JsonProperty("title")
    private String title;

}
