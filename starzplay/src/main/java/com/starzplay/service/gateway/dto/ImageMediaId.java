
package com.starzplay.service.gateway.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonAutoDetect
public class ImageMediaId {

    @JsonProperty("mediaId")
    private String mediaId;
    @JsonProperty("isPrimary")
    private Boolean isPrimary;
    @JsonProperty("imageType")
    private String imageType;

}
