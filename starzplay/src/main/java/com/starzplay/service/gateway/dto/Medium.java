
package com.starzplay.service.gateway.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonAutoDetect
public class Medium {

    @JsonProperty("id")
    private String id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("guid")
    private String guid;
    @JsonProperty("ownerId")
    private String ownerId;
    @JsonProperty("availableDate")
    private Long availableDate;
    @JsonProperty("expirationDate")
    private Long expirationDate;
    @JsonProperty("ratings")
    public List<Object> ratings = null;
    @JsonProperty("content")
    private List<Content> content = null;
    @JsonProperty("restrictionId")
    private String restrictionId;
    @JsonProperty("availabilityState")
    private String availabilityState;

}
