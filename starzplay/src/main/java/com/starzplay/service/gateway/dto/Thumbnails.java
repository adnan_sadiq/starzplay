
package com.starzplay.service.gateway.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonAutoDetect
public class Thumbnails {

    @JsonProperty("thumb-615x1536")
    private Thumb615x1536 thumb615x1536;
    @JsonProperty("thumb-677x474")
    private Thumb677x474 thumb677x474;
    @JsonProperty("thumb-613x1536")
    private Thumb613x1536 thumb613x1536;
    @JsonProperty("thumb-614x1536")
    private Thumb614x1536 thumb614x1536;

}
