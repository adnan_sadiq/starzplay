
package com.starzplay.service.gateway.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonAutoDetect
public class ContentMetadataDTO {

    @JsonProperty("$xmlns")
    private $xmlns $xmlns;
    @JsonProperty("startIndex")
    private Integer startIndex;
    @JsonProperty("itemsPerPage")
    private Integer itemsPerPage;
    @JsonProperty("entryCount")
    private Integer entryCount;
    @JsonProperty("title")
    private String title;
    @JsonProperty("entries")
    private List<Entry> entries = null;

}
