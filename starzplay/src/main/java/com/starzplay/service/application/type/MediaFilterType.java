package com.starzplay.service.application.type;

/**
 * Created by Adnan Sadiq on 4/1/2018.
 */
public enum MediaFilterType {

    CENSORING();

    public static MediaFilterType getFilterType(String filter) {

        switch (filter.toUpperCase()) {
            case "CENSORING":
                return MediaFilterType.CENSORING;
            default:
                throw new IllegalArgumentException("No filter type found against input value " + filter);
        }
    }
}
