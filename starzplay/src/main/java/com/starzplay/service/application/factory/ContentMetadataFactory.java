package com.starzplay.service.application.factory;

import com.starzplay.service.ContentMetadataService;
import com.starzplay.service.application.type.MediaLevelType;
import com.starzplay.service.impl.CensoredMetadataServiceImpl;
import com.starzplay.service.impl.UncensoredMetadataServiceImpl;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.EnumMap;
import java.util.Map;

/**
 * Created by Adnan Sadiq.
 */
@Component
public class ContentMetadataFactory implements ApplicationContextAware, InitializingBean {

    private ApplicationContext applicationContext;

    private Map<MediaLevelType, ContentMetadataServiceResolver> resolvers;

    @FunctionalInterface
    private interface ContentMetadataServiceResolver {
        ContentMetadataService resolveContentsMetadataService();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        this.resolvers = new EnumMap<>(MediaLevelType.class);
        this.resolvers.put(MediaLevelType.CENSORED, () -> this.applicationContext.getBean(CensoredMetadataServiceImpl.class));
        this.resolvers.put(MediaLevelType.UNCENSORED,
                () -> this.applicationContext.getBean(UncensoredMetadataServiceImpl.class));
    }

    public ContentMetadataService getContentMetadataService(MediaLevelType mediaLevelType) {

        ContentMetadataServiceResolver contentMetadataServiceResolver = this.resolvers.get(mediaLevelType);

        if (contentMetadataServiceResolver != null) {
            return contentMetadataServiceResolver.resolveContentsMetadataService();
        } else {
            throw new IllegalStateException(
                    "No ContentMetadataServiceResolver defined for mediaLevelType=" + mediaLevelType);
        }
    }


}
