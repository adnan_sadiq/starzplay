package com.starzplay.service.application.type;

/**
 * Created by Adnan Sadiq on 4/1/2018.
 */
public enum MediaLevelType {

    UNCENSORED(), CENSORED();

    public static MediaLevelType getLevelType(String level) {

        switch (level.toUpperCase()) {
            case "UNCENSORED":
                return MediaLevelType.UNCENSORED;
            case "CENSORED":
                return MediaLevelType.CENSORED;
            default:
                throw new IllegalArgumentException("No level type found against input value " + level);
        }
    }
}
