package com.starzplay.service.impl;

import com.starzplay.service.ContentMetadataService;
import com.starzplay.service.application.type.MediaLevelType;
import com.starzplay.service.gateway.ContentMetadataGateway;
import com.starzplay.service.gateway.dto.Entry;
import com.starzplay.service.gateway.dto.Medium;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by hp on 4/1/2018.
 */
public abstract class ContentMetadataBaseService implements ContentMetadataService {

    private final String CENSORED_GUID_ENDING_CHAR = "C";

    @Autowired
    ContentMetadataGateway contentMetadataGatway;

    protected boolean isMovieCensored(Entry entry) {

        return !StringUtils.isEmpty(entry.getPeg$contentClassification()) &&
                entry.getPeg$contentClassification().equalsIgnoreCase(MediaLevelType.CENSORED.name());
    }


    protected void filterMedia(Entry entry, MediaLevelType mediaLevelType) {

        List<Medium> filterMedia = entry.getMedia().stream().filter(media -> {

            if (media.getGuid().endsWith(CENSORED_GUID_ENDING_CHAR)) {
                return mediaLevelType.equals(MediaLevelType.CENSORED);
            } else {
                return mediaLevelType.equals(MediaLevelType.UNCENSORED);
            }

        }).collect(Collectors.toList());

        entry.setMedia(filterMedia);
    }


}
