package com.starzplay.service.impl;

import com.starzplay.service.ContentMetadataService;
import com.starzplay.service.application.type.MediaFilterType;
import com.starzplay.service.application.type.MediaLevelType;
import com.starzplay.service.gateway.dto.ContentMetadataDTO;
import com.starzplay.service.gateway.dto.Entry;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by hp on 4/1/2018.
 */
@Service
public class UncensoredMetadataServiceImpl extends ContentMetadataBaseService {

    @Override
    public ContentMetadataDTO filterContents(MediaFilterType mediaFilterType) {

        ContentMetadataDTO contentMetadataDTO = this.contentMetadataGatway.getContentsMetadata();
        contentMetadataDTO.getEntries().stream().filter(entry -> this.isMovieCensored(entry)).forEach(entry ->
                this.filterMedia(entry, MediaLevelType.UNCENSORED)
        );
        return contentMetadataDTO;
    }
}