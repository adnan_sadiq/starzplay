package com.starzplay.service;

import com.starzplay.service.application.type.MediaFilterType;
import com.starzplay.service.gateway.dto.ContentMetadataDTO;

/**
 * Created by Adnan Sadiq on 4/1/2018.
 */
public interface ContentMetadataService {

    ContentMetadataDTO filterContents(MediaFilterType mediaFilterType);

}
