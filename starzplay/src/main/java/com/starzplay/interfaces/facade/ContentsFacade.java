package com.starzplay.interfaces.facade;

import com.starzplay.service.application.type.MediaFilterType;
import com.starzplay.service.application.type.MediaLevelType;
import com.starzplay.service.gateway.dto.ContentMetadataDTO;

/**
 * Created by Adnan Sadiq on 4/1/2018.
 */
public interface ContentsFacade {

    ContentMetadataDTO getContentsMetadata(MediaLevelType mediaLevelType, MediaFilterType mediaFilterType);
}
