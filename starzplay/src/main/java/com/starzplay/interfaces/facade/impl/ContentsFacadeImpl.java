package com.starzplay.interfaces.facade.impl;

import com.starzplay.interfaces.facade.ContentsFacade;
import com.starzplay.service.application.factory.ContentMetadataFactory;
import com.starzplay.service.application.type.MediaFilterType;
import com.starzplay.service.application.type.MediaLevelType;
import com.starzplay.service.gateway.dto.ContentMetadataDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Adnan Sadiq.
 */
@Service
public class ContentsFacadeImpl implements ContentsFacade {

    @Autowired
    ContentMetadataFactory metadataFactory;

    @Override
    public ContentMetadataDTO getContentsMetadata(MediaLevelType mediaLevelType, MediaFilterType mediaFilterType) {

        return metadataFactory.getContentMetadataService(mediaLevelType).filterContents(mediaFilterType);

    }
}
