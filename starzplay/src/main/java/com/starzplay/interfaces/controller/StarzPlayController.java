package com.starzplay.interfaces.controller;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.starzplay.interfaces.facade.ContentsFacade;
import com.starzplay.service.application.type.MediaFilterType;
import com.starzplay.service.application.type.MediaLevelType;
import com.starzplay.service.gateway.dto.ContentMetadataDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Adnan Sadiq.
 */
@RequestMapping("v{apiVersion}")
@RestController
public class StarzPlayController {

    @Autowired
    ContentsFacade contentsFacade;

    @GetMapping("/media")
    public JsonNode getMedia(
            @RequestParam String filter,
            @RequestParam String level
    ) {
        ContentMetadataDTO contentMetadataDTO = contentsFacade.getContentsMetadata(MediaLevelType.getLevelType(level), MediaFilterType.getFilterType(filter));
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.valueToTree(contentMetadataDTO);

    }
}
